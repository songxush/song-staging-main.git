"use strict"
module.exports = core
// require 能加载 .js .json .node
// .js ---> module.exports/exports
// .json ---> Json.pase
// any 转化 成 ---> .js 
const path = require('path')
const {Command} = require('commander'); //
const semver = require('semver')
const colors = require("colors/safe")
const userHome = require("user-home")
const pkg = require("../package.json");//
const log = require("@song-staging-dev/log")
const init = require("@song-staging-dev/init")
const constant = require("./const.js")
const rootCheck = require('root-check');
let args;
const program = new Command();//实例化脚手架对象
async function core() {
    try {
        // 命令提示
        checkPkyVersion()
        checkNodeVersion()
        checkRoot()
        checkUserHome()
        // checkInputArgs()
        checkEnv()
        await checkGlobalUpdate()
        reqisterCommand()
    } catch (e) {
        log.error(e.message)
    }
}
// 命令注册函数
function registerCommand() {
    program
        .name(Object.keys(pkg.bin)[0])
        .usage("<command> [options]")
        .version(pkg.version)
        .option('-d,--debug', '是否开启调试模式', false)
    program.on('option:debug', function () {
        if (this.opts().debug) {
            process.env.LOG_LEVEL = "verbose"
        } else {
            process.env.LOG_LEVEL = "info"
        }
        // log.level  改为 log.level = 'verbose' 模式
        log.level = process.env.LOG_LEVEL
        log.verbose('test 测试代码')
    })
    // 当前没有
    program.on('command:*',function(obj) {
        console.log(this.opts())
    })
    program.parse(process.argv);
}
// 检查版本号
function checkPkyVersion() {
    // log.info("cli", pkg.version)
}
// 检查Node版本号 
function checkNodeVersion() {
    // 获取当前 Node 版本号
    const currentVersion = process.version
    // 对比最低版本号
    const lowestVersion = constant.LOWEST_VERSION
    if (!semver.gte(currentVersion, lowestVersion)) {
        throw new Error(colors.red(`song-stagin-dev 需要安装 V${lowestVersion} 以上的版本Node.js`))
    }
}
// 检测root 账户
function checkRoot() {
    rootCheck()
}
// 检查用户主目录
function checkUserHome() {
    if (!userHome) {
        throw new Error(colors.red("当前登录用户主目录不存在"))
    }
}
// 检查入参 判断是那种模式下运行 debug 模式下
function checkInputArgs() {
    const minimist = require("minimist")
    args = minimist(process.argv.slice(2))
    checkArgs()
}
function checkArgs() {
    if (args.debug) {
        process.env.LOG_LEVEL = "verbose"
    } else {
        process.env.LOG_LEVEL = "info"
    }
    // log.level  改为 log.level = 'verbose' 模式
    log.level = process.env.LOG_LEVEL
}
// 检测环境变量检查功能
function checkEnv() {
    createDefaultConfig()
    log.verbose("环境变量", process.env.CLI_HOME_PATH)
}
// 默认配置 环境变量
function createDefaultConfig() {
    const cliConfig = {
        home: userHome,
    }
    // 判断环境变量是否有CLI_HOME 如果有调用 没有的话从新编写
    if (process.env.CLI_HOME) {
        cliConfig['cliHome'] = path.join(userHome, process.env.CLI_HOME)
    } else {
        cliConfig['cliHome'] = path.join(userHome, constant.DEFAULT_CLI_HOME)
    }
    process.env.CLI_HOME_PATH = cliConfig.cliHome
}
// 检查是不是最新版本
async function checkGlobalUpdate() {
    // 1. 获取当前版本号和模块名称
    const currentVersion = pkg.version;
    const npmName = pkg.name;
    //2. 调用 npm API  获取所有的版本号
    const { getNpmInfo, getNpmVersions, getNpmSemverVersion } = require("@song-staging-dev/get-npm-info")
    const lastVersions = await getNpmSemverVersion(currentVersion, npmName)
    if (lastVersions && semver.gt(lastVersions, currentVersion)) {
        log.warn(colors.yellow(`请手动更新${npmName},当前版本:${currentVersion},最新版本:${lastVersions}`))
    }
    // 3. 提取所有版本号，对比那些版本号大于当前版本号
    // 4. 获取最新的版本号 提示用户更新到该版本
}
// 命令注册
function reqisterCommand () {
    program
        .name(Object.keys(pkg.bin)[0])
        .usage("<command> [options]")
        .version(pkg.version)
        .option('-d,--debug','是否开启调试模式',false);
    program
        .command('init [projectName]')
        .option('-f,--force','是否强制初始化项目')
        .action(init)
    // 开启debug模式
    program.on('option:debug',function() {
        if(this.opts().debug) {
            process.env.LOG_LEVEL = "verbose"
        }else {
            process.env.LOG_LEVEL = "info"
        }
        log.level = process.env.LOG_LEVEL
        log.verbose('test 开启调试模式')
    })
    // 其他命令监听
    program.on('command:*',function(obj){
        const avaliableCommands = program.options.map(cmd => cmd.name())
        console.log(colors.red('未知命令:' + obj[0]));
        console.log(colors.red('可用命令：' + avaliableCommands.join(',')))
    })
    program.parse(process.argv);  
    // 当没有输入的时候打印帮助文档
    if(program.args && program.args.length < 1) {
        program.outputHelp()
    }else {
       
    }

}
