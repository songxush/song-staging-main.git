#!/usr/bin/env node
const importLocal = require("import-local")//用来判断使用本地node_modules or 线下
if(importLocal(__filename)){
    require('npmlog').info("cli","正在使用song-staging-dev本地版本") //走线上node_modules 代码
}else{
    require("../lib")(process.argv.slice(2)) // 走本地代码
}

