'use strict';
const log = require('npmlog')
log.level = process.env.LOG_LEVEL ? process.env.LOG_LEVEL : 'info'
log.addLevel('list', 2000, { fg: 'green', bg: 'black',bold:true })
log.addLevel('debug', 1000, { fg: 'green', bg: 'black' }, 'debug')
module.exports = log;

